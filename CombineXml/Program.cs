﻿using System;
using System.IO;
using System.Linq;
using System.Xml;

namespace CombineXml
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Path to xml files: ");
            var path = Console.ReadLine();

            CombineLogs(path);
            Console.Read();
        }

        static private void CombineLogs(string logPath)
        {
            if (!Directory.Exists(logPath))
            {
                Console.WriteLine("File not found");
                return;
            }

            var listOfFiles = Directory.GetFiles(logPath, "*.xml").ToList();

            var mainDoc = new XmlDocument();

            var setMain = false;
            foreach (var file in listOfFiles)
            {
                if (!setMain)
                {
                    mainDoc.Load(file);
                    setMain = true;
                    continue;
                }

                Console.WriteLine("Loading doc: " + file);
                var doc = new XmlDocument();
                doc.Load(file);

                var features = doc.SelectNodes("//test-suite[@name='Features']/results");

                if (features == null)
                    throw new Exception("no features found");

                foreach (XmlNode feature in features)
                {
                    if (feature.FirstChild.Attributes == null)
                        throw new Exception("feature not found");

                    var curFeature = feature.FirstChild.Attributes["name"].Value;

                    Console.WriteLine("Feature: " + curFeature);
                    if (mainDoc.SelectSingleNode("//test-suite[@name='" + curFeature + "']") != null)
                    {
                        if (feature.FirstChild.FirstChild.FirstChild.Attributes == null)
                            throw new Exception("sub feature not found");

                        var curSubFeature = feature.FirstChild.FirstChild.FirstChild.Attributes["name"].Value;
                        Console.WriteLine("Sub Feature: " + curSubFeature);
                        var curNode = mainDoc.SelectSingleNode("//test-suite[@name='" + curSubFeature + "']");

                        if (curNode != null)
                        {
                            curNode.InnerXml += feature.FirstChild.FirstChild.FirstChild.InnerXml;
                        }
                        else
                        {
                            mainDoc.SelectSingleNode("//test-suite[@name='" + curFeature + "']").InnerXml += feature.FirstChild.InnerXml;
                        }
                    }
                    else
                    {
                        mainDoc.SelectSingleNode("//test-suite[@name='Features']/results").InnerXml += feature.InnerXml;
                    }
                }
            }

            var combinedPath = new FileInfo(logPath + @"\out\combined.xml");


            if (!combinedPath.Exists)
            {
                if (!combinedPath.Directory.Exists)
                    combinedPath.Directory.Create();

                combinedPath.Create();
            }
            mainDoc.Save(combinedPath.FullName);
        }
    }
}
